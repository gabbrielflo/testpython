from login import auth_required
from flask import Flask, json, jsonify,request

app = Flask(__name__)

@app.route('/datos', methods = ['POST'])
@auth_required
def ordenList():
    input_data={
        "sin clasificar": request.json["sin clasificar"]
        }
    num_not_duplicate = [] 
    num_duplicate = []    
    for i in (input_data["sin clasificar"]):
        if i not in num_not_duplicate:
            num_not_duplicate.append(i)
        else:
            if i not in num_duplicate:
                num_duplicate.append(i)
    list_ordered= sorted(num_not_duplicate)+num_duplicate
    date_output={ 
        "sin clasificar": (str(request.json["sin clasificar"])),
        "clasificado": (str(list_ordered))
    }
    return jsonify(date_output)

if __name__ == '__main__':
    app.run(debug=True, port=3000)